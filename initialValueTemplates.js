import T from '@sanity/base/initial-value-template-builder'

export default [
  ...T.defaults(),

  T.template({
    id: 'collection-page',
    title: 'Collection Page',
    schemaType: 'page',
    parameters: [{ name: 'collectionId', type: 'string' }],
    value: params => ({
      collection: { _type: 'reference', _ref: params.collectionId }
    })
  })
]