// First, we must import the schema creator
import createSchema from 'part:@sanity/base/schema-creator'

// Then import schema types from any plugins that might expose them
import schemaTypes from 'all:part:@sanity/base/schema-type'

// document schemas
import brand from './documents/brand'
import category from './documents/category'
import collection from './documents/collection'
import collectionList from './documents/collectionList'
import customItem from './documents/customItem'
import customList from './documents/customList'
import hubspotForm from './documents/hubspotForm'
import page from './documents/page'
import pageSection from './documents/pageSection'
import product from './documents/product'
import schemeColor from './documents/schemeColor'
import site from './documents/site'
import styles from './documents/styles'

// Object types
import address from './objects/address'
import background from './objects/background'
import content from './objects/content'
import cta from './objects/cta'
import font from './objects/font'
import fonts from './objects/fonts'
import footer from './objects/footer'
import hero from './objects/hero'
import itemLayout from './objects/itemLayout'
import link from './objects/link'
import links from './objects/links'
import list from './objects/list'
import listLayout from './objects/listLayout'
import logos from './objects/logos'
import mainImage from './objects/mainImage'
import menu from './objects/menu'
import menuItem from './objects/menuItem'
import organisation from './objects/organisation'
import preview from './objects/preview'
import sectionLayout from './objects/sectionLayout'
import socialLink from './objects/socialLink'

// Then we give our schema to the builder and provide the result to Sanity
export default createSchema({
  // We name our schema
  name: 'default',
  // Then proceed to concatenate our document type
  // to the ones provided by any plugins that are installed
  types: schemaTypes.concat([
    /* Your types here! */

    // documents
    brand,
    category,
    collection,
    collectionList,
    customItem,
    customList,
    hubspotForm,
    page,
    pageSection,
    product,
    schemeColor,
    site,
    styles,

    // objects
    address,
    background,
    content,
    cta,
    font,
    fonts,
    footer,
    hero,
    itemLayout,
    link,
    links,
    list,
    listLayout,
    logos,
    mainImage,
    menu,
    menuItem,
    organisation,
    preview,
    sectionLayout,
    socialLink
    
  ])
})
