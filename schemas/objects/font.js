export default {
    name: 'font',
    title: 'Font',
    type: 'object',
    fields: [
        {
            name: 'name',
            title: 'Name',
            type: 'string'
        },
        {
            name: 'src',
            title: 'Source',
            type: 'url'
        }
    ]
}