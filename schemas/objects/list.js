export default {
  name: 'list',
  title: 'List',
  type: 'object',
  fields: [
    {
      name: 'list',
      type: 'reference',
      to: [{ type: 'customList' }, { type: 'collectionList' }]
    },
    {
      name: 'layout',
      type: 'listLayout'
    }
  ]
}