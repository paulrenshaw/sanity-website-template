export default {
    name: 'preview',
    title: 'Preview',
    type: 'object',
    fields: [
        {
            name: 'description',
            title: 'Description',
            type: 'string',
            description: 'A short description for previews on other pages and shared links.'
        },
        {
            name: 'mainImage',
            title: 'Image',
            type: 'mainImage'
        }
    ]
}
