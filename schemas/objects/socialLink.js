export default {
  name: 'socialLink',
  type: 'object',
  title: 'Social Link',
  fields: [
    {
      name: 'url',
      title: 'Social URL',
      type: 'url'
    },
    {
      name: 'icon',
      title: 'Font Awesome Icon',
      type: 'string'
    }
  ]
}
