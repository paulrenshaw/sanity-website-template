export default {
    name: 'footer',
    title: 'Footer',
    type: 'object',
    fields: [
        {
            name: 'linkLists',
            title: 'Link Lists',
            type: 'menu'
        },
        {
            name: 'background',
            title: 'Background',
            type: 'background'
        }
    ]
}