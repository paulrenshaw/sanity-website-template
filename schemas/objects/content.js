export default {
    name: 'content',
    title: 'Content',
    type: 'object',
    fields: [
        {
            name: 'content',
            type: 'array',
            of: [
                { 
                    type: 'block',
                    styles: [
                        { title: 'Normal', value: 'normal' },
                        { title: 'Heading', value: 'h3' },
                        { title: 'SubHeading', value: 'h4' },
                        { title: 'Quote', value: 'blockquote' }
                    ]
                }, 
                { type: 'mainImage' }
            ]
        }
    ]
}
