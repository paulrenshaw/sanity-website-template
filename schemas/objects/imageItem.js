export default {
  name: 'imageItem',
  title: 'Image Item',
  type: 'image',
  fields: [
    {
      name: 'alt',
      type: 'string'
    },
    {
      name: 'style',
      type: 'string',
      options: {
        list: ['contain', 'cover']
      }
    },
    {
      name: 'hasMargin',
      type: 'boolean'
    }
  ]
}
