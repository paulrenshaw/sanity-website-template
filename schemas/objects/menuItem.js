export default {
    name: 'menuItem',
    title: 'Menu Item',
    type: 'object',
    fields: [
        {
            name: 'title',
            title: 'Title',
            type: 'string'
        },
        {
            name: 'default',
            title: 'Title Link',
            type: 'array',
            of: [
                {
                    title: 'Page Link',
                    type: 'reference',
                    to: [{ type: 'page' }]
                },
                {
                    title: 'Custom Link',
                    type: 'link'
                }
            ]
        },
        {
            name: 'menu',
            title: 'Sub Menu',
            type: 'array',
            of: [
                {
                    title: 'Page Link',
                    type: 'reference',
                    to: [{ type: 'page' }],
                    validation: Rule => Rule.required()
                },
                {
                    title: 'Custom Link',
                    type: 'link'
                }
            ],
        }
    ]
}