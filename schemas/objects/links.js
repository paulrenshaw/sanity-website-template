export default {
  name: "links",
  type: "object",
  title: "Links",
  fields: [
    {
      name: "links",
      title: "Links",
      type: "array",
      of: [
        {
          title: "Page Link",
          type: "reference",
          to: [{ type: "page" }],
          validation: (Rule) => Rule.required(),
        },
        {
          title: "Custom Link",
          type: "link",
        },
      ],
    },
  ],
};
