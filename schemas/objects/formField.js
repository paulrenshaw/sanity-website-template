export default {
    name: 'formField',
    title: 'Form Field',
    type: 'object',
    fields: [
        {
            name: 'label',
            title: 'Label',
            type: 'string'
        },
        {
            name: 'input',
            title: 'Input',
            type: 'array',
            of: [
                { 
                    title: 'Text',
                    type: 'textInput' 
                },
                {
                    title: 'Email',
                    type: 'emailInput'
                },
                {
                    title: 'Telephone',
                    type: 'telephoneInput'
                },
                {
                    Title: 'Text Area',
                    type: 'textarea'
                }
            ],
            validation: Rule => Rule.required().length(1)
        }
    ]
}