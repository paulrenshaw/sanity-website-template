export default {
    name: 'address',
    title: 'Address',
    type: 'object',
    fields: [
        {
            name: 'name',
            title: 'Building / Flat / Suite Name',
            type: 'string'
        },
        {
            name: 'street',
            title: 'Street',
            type: 'string',
            description: 'Should include the building number if a numbered address.'
        },
        {
            name: 'locality',
            title: 'Locality / Additional Line',
            type: 'string'
        },
        {
            name: 'city',
            title: 'Town / City',
            type: 'string'
        },
        {
            name: 'county',
            title: 'County / State',
            type: 'string'
        },
        {
            name: 'code',
            title: 'Postal / Zip Code',
            type: 'string'
        },
        {
            name: 'country',
            title: 'Country',
            type: 'string'
        }
    ]
}