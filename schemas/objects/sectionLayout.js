export default {
  name: 'sectionLayout',
  title: 'Section Layout',
  type: 'object',
  fields: [
    {
      name: 'fullWidth',
      title: 'Full Width Section',
      type: 'boolean'
    }
  ]
}