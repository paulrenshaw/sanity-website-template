export default {
  name: 'link',
  type: 'object',
  title: 'Link',
  fields: [
    {
      name: 'title',
      type: 'string',
      title: 'Title'
    },
    {
      name: 'path',
      type: 'url',
      title: 'URL Path',
      validation: Rule =>
        Rule.uri({
          allowRelative: true
        })
    }
  ]
}
