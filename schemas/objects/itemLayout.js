export default {
  name: 'itemLayout',
  title: 'Item Layout',
  type: 'object',
  fields: [
    {
      name: 'style',
      type: 'string',
      options: {
        list: ['plain', 'card']
      }
    },
    {
      name: 'imagePosition',
      type: 'string',
      options: {
        list: ['top', 'left', 'right', 'bottom']
      }
    },
    {
      name: 'textAlign',
      title: 'Text Alignment',
      type: 'string',
      options: {
        list: ['left', 'center', 'right']
      }
    }
  ]
}