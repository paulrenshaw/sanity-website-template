export default {
    name: 'fonts',
    title: 'Fonts',
    type: 'object',
    fields: [
        {
            name: 'primary',
            title: 'Primary',
            type: 'font'
        },
        {
            name: 'secondary',
            title: 'Secondary',
            type: 'font'
        }
    ]
}