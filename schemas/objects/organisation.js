export default {
    name: 'organisation',
    title: 'Company / Organisation',
    type: 'object',
    fields: [
        {
            name: 'name',
            title: 'Name',
            type: 'string',
            description: 'The full official company or organisation name e.g. Acme Inc/Ltd/LLC or personal name if a sole trader.'
        },
        {
            name: 'number',
            title: 'Registration Number',
            type: 'string',
            description: 'Official registration number e.g. Companies House number.'
        },
        {
            name: 'location',
            title: 'Registration Location',
            type: 'string',
            description: 'Official registration location e.g. England and Wales'
        },
        {
            name: 'address',
            title: 'Address',
            type: 'address'
        }
    ]
}