export default {
  name: "background",
  title: "Background",
  type: "object",
  fields: [
    {
      name: "image",
      title: "Image",
      type: "mainImage",
    },
    {
      name: "colors",
      title: "Color(s)",
      type: "array",
      of: [
        {
          type: "reference",
          to: [{ type: 'schemeColor' }]
        },
      ],
    },
    {
      name: "gradient",
      title: "Gradient",
      type: "string",
      options: {
        list: [
          "to top left",
          "to top",
          "to top right",
          "to left",
          "to right",
          "to bottom left",
          "to bottom",
          "to bottom right",
        ],
      },
      description: "Only applies if 2 or more colors are chosen.",
    },

    {
      name: "blend",
      type: "boolean",
    },
    {
      name: "clip",
      type: "boolean",
    },
    {
      name: "contain",
      type: "boolean",
    },
    {
      name: "fixed",
      type: "boolean",
    },
  ],
};
