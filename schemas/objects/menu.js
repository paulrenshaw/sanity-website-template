export default {
    name: 'menu',
    title: 'Menu',
    type: 'object',
    fields: [
        {
            name: 'items',
            title: 'Items',
            type: 'array',
            of: [{ type: 'menuItem' }]
        }
    ]
}