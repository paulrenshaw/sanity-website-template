export default {
  name: 'listLayout',
  title: 'List Layout',
  type: 'object',
  fields: [
    {
      name: 'itemsPerRow',
      title: 'Items Per Row',
      type: 'number'
    },
    {
      name: 'fluidItems',
      title: 'Fluid Items',
      type: 'boolean',
      description: 'If there are less items on a row than Items Per Row, the items will expand to fill the space.'
    }
  ]
}