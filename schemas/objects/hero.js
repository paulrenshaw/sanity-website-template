export default {
    name: 'hero',
    title: 'Hero',
    type: 'object',
    fields: [
        {
            name: 'title',
            title: 'Title',
            type: 'string'
        },
        {
            name: 'subtitle',
            title: 'Subtitle',
            type: 'string'
        },
        {
            name: 'cta',
            title: 'Call to Action',
            type: 'cta'
        },
        {
            name: 'background',
            title: 'Background',
            type: 'background'
        }
    ]
}