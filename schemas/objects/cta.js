export default {
  name: 'cta',
  title: 'Call to Action',
  type: 'object',
  fields: [
    {
      name: 'link',
      type: 'link'
    },
    {
      name: 'color',
      type: 'reference',
      to: [{ type: 'schemeColor' }]
    }
  ]
}
