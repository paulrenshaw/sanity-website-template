export default {
    name: 'logos',
    title: 'Logos',
    type: 'object',
    fields: [
        {
            name: 'default',
            title: 'Main Logo',
            type: 'image'
        },
        {
            name: 'icon',
            title: 'Square Logo / Icon',
            type: 'image',
            description: 'Should be a 512 x 512 pixels square image to be used as the site\'s icon.'
        }
    ]
}