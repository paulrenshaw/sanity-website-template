export default {
    name: 'colourSelector',
    title: 'Colour Selector',
    type: 'object',
    fields: [
        {
            name: 'colour',
            title: 'Colour',
            type: 'string',
            options: {
                list: ['primary', 'secondary', 'light', 'dark', 'alt']
            }
        }
    ]
}