export default {
  name: "mainImage",
  type: "image",
  title: "Image",
  fields: [
    {
      name: "alt",
      type: "string",
      title: "Alternative text",
      description: "Important for SEO and accessibility.",
      validation: (Rule) =>
        Rule.error("You have to fill out the alternative text.").required(),
      options: {
        isHighlighted: true,
      },
    },
    {
      name: "caption",
      type: "string",
      title: "Caption",
      options: {
        isHighlighted: true,
      },
    },
  ],
  options: {
    hotspot: true,
  },
  preview: {
    select: {
      imageUrl: "asset.url",
      title: "alt",
    },
  },
};
