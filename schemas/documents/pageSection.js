export default {
  name: 'pageSection',
  title: 'Page Section',
  type: 'document',
  fields: [
    {
      name: 'heading',
      title: 'Heading',
      type: 'string'
    },
    {
      name: 'isH1',
      title: 'Is main page heading?',
      type: 'boolean',
      description: 'You should only set this to true for one section per page.'
    },
    {
      name: 'content',
      title: 'Main Content',
      type: 'content',
      description: 'Main rich text content displayed above items.'
    },
    {
      name: 'list',
      type: 'list'
    },
    {
      name: 'background',
      title: 'Background',
      type: 'background'
    },
    {
      name: 'color',
      title: 'Font Color',
      type: 'reference',
      to: [{ type: 'schemeColor' }]
    },
    {
      name: 'layout',
      type: 'sectionLayout'
    }
  ],
  preview: {
    select: {
      title: 'heading'
    }
  }
}