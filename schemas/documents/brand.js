export default {
    name: 'brand',
    title: 'Brand / Company / Copyright Info',
    type: 'document',
    fields: [
        {
            name: 'name',
            title: 'Brand Name',
            type: 'string'
        },
        {
            name: 'organisation',
            title: 'Company / Organisation',
            type: 'organisation'
        },
        {
            name: 'logos',
            title: 'Logos',
            type: 'logos'
        },
        {
            name: 'social',
            title: 'Social Links',
            type: 'array',
            of: [{ type: 'socialLink' }]
        }
    ]
}
