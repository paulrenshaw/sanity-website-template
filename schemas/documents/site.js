export default {
    name: 'site',
    title: 'Site',
    type: 'document',
    fields: [
        {
            name: 'menu',
            title: 'Main Navigation Menu',
            type: 'menu'
        },
        {
            name: 'footer',
            title: 'Footer',
            type: 'footer'
        }
    ]
}