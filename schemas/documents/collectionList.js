export default {
  name: "collectionList",
  title: "Collection List",
  type: "document",
  fields: [
    {
      name: "collection",
      title: "Collection",
      type: "reference",
      to: [{ type: "collection" }],
    },
    {
      name: "category",
      type: "reference",
      to: [{ type: "category" }],
    },
    {
      name: "featured",
      title: "Featured Only",
      type: "boolean",
    },
    {
      name: "limit",
      title: "Max Number of Items",
      type: "number",
    }
  ],
  preview: {
    select: {
      title: "collection.title",
      featured: "featured",
      category: "category.title",
    },
    prepare(selection) {
      const { title, featured, category } = selection;
      return {
        title: `${featured ? "Featured " : ""}${
          category ? category + " " : ""
        }${title} Items`,
      };
    },
  },
};
