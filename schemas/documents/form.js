export default {
    name: 'form',
    title: 'Form',
    type: 'document',
    fields: [
        {
            name: 'title',
            title: 'Title',
            type: 'string'
        },
        {
            name: 'description',
            title: 'Description',
            type: 'string'
        },
        {
            name: 'fields',
            title: 'Fields',
            type: 'array',
            of: [
                { type: 'formField'}
            ]
        }
    ]
}
