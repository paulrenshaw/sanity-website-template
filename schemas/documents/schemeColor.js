export default {
  name: 'schemeColor',
  title: 'Color',
  type: 'document',
  fields: [
    {
      name: 'name',
      type: 'string'
    },
    {
      name: 'color',
      type: 'color'
    }
  ]
}
