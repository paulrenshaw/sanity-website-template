export default {
    name: 'hubspotForm',
    title: 'Hubspot Form',
    type: 'document',
    fields: [
        {
            name: 'title',
            title: 'Title',
            type: 'string'
        },
        {
            name: 'portalId',
            title: 'Portal ID',
            type: 'string'
        },
        {
            name: 'formId',
            title: 'Form ID',
            type: 'string'
        }
    ]
}