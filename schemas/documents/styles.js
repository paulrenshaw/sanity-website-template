export default {
  name: "styles",
  title: "Styles",
  type: "document",
  fields: [
    {
      name: "fonts",
      title: "Fonts",
      type: "fonts",
    },
  ],
};
