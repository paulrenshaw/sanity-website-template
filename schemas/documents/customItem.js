export default {
    name: 'customItem',
    title: 'Custom Item',
    type: 'document',
    fields: [
        {
            name: 'image',
            type: 'mainImage'
        },
        {
            name: 'content',
            title: 'Content',
            type: 'array',
            of: [
                { 
                    type: 'block',
                    styles: [
                        { title: 'Normal', value: 'normal' },
                        { title: 'Heading', value: 'h3' },
                        { title: 'SubHeading', value: 'h4' },
                        { title: 'Quote', value: 'blockquote' }
                    ]
                }
            ]
        },
        {
            name: 'cta',
            title: 'Call to Action',
            type: 'cta'
        },
        {
            name: 'background',
            type: 'background'
        },
        {
            name: 'layout',
            type: 'itemLayout'
        }
    ],
    initialValue: {
        imagePosition: 'top'
    }
}
