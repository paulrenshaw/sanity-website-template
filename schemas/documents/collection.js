export default {
    name: 'collection',
    title: 'Collection',
    type: 'document',
    fields: [
        {
            name: 'title',
            title: 'Title',
            type: 'string'
        },
        {
            name: 'slug',
            title: 'Slug',
            type: 'slug',
            options: {
                source: doc => doc.title
            }
        }
    ],
    preview: {
        select: {
            title: 'title',
            slug: 'slug.current'
        },
        prepare(selection) {
            const { title, slug } = selection
            return {
                title,
                subtitle: `/${slug}`
            }
        }
    }
}