export default {
  name: 'settings',
  type: 'document',
  title: 'Settings',
  __experimental_actions: ['update', /* 'create', 'delete', */ 'publish'],
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string'
    },
    {
      name: 'description',
      title: 'Description',
      type: 'string'
    },
    {
      name: 'logo',
      title: 'Logo',
      type: 'mainImage'
    },
    {
      name: 'menu',
      title: 'Main Navigation Menu',
      type: 'menu'
    },
    {
      name: 'footer',
      title: 'Footer',
      type: 'footer'
    },
    {
      name: 'colors',
      title: 'Colour Scheme',
      type: 'array',
      of: [{ type: 'color' }],
    }
  ]
}
