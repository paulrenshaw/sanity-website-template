export default {
  name: 'video',
  type: 'document',
  title: 'Videos',
  fields: [
    {
      name: 'title',
      type: 'string',
      title: 'Title'
    },
    {
      name: 'url',
      type: 'url',
      title: 'Video URL'
    }
  ]
}
