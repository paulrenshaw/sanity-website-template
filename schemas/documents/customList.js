export default {
  title: 'Custom List',
  name: 'customList',
  type: 'document',
  fields: [
    {
      name: 'title',
      type: 'string'
    },
    {
      name: 'items',
      title: 'Items',
      type: 'array',
      of: [
        { title: 'Custom Item', type: 'customItem' },
        { title: 'Page Preview', type: 'reference', to: [{ type: 'page' }] }
      ]
    }
  ]
}
