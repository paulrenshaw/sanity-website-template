export default {
  name: 'page',
  type: 'document',
  title: 'Page',
  fields: [
    {
      name: 'collection',
      title: 'Collection',
      type: 'reference',
      to: [{ type: 'collection' }]
    },
    {
      name: 'category',
      title: 'Category',
      type: 'reference',
      to: [{ type: 'category' }]
    },
    {
      name: 'featured',
      type: 'boolean'
    },
    {
      name: 'title',
      title: 'Title',
      type: 'string',
      description: 'Title for search results and browser tab/window. Not displayed on the page.',
      validation: Rule => Rule.required().max(60)
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: doc => doc.title
      },
      description: 'Used to determine the path to the page in the URL e.g. example.com/slug-goes-here'
    },
    {
      name: 'sections',
      title: 'Sections',
      type: 'array',
      of: [{ type: 'pageSection' }]
    },
    {
        name: 'description',
        title: 'Preview Description',
        type: 'string',
        description: 'Description for search results and previews.',
        validation: Rule => Rule.required().max(160)
    },
    {
      name: 'image',
      title: 'Preview Image',
      type: 'mainImage'
    },
    {
      name: 'background',
      title: 'Background',
      type: 'background'
    }
  ],
  preview: {
    select: {
      collection: 'collection.slug.current',
      title: 'title',
      slug: 'slug.current',
      description: 'description',
      media: 'image'
    },
    prepare(selection) {
      const { collection, title, slug, description, media } = selection
      return {
        title: `${title} ${slug ? '(/' + (collection ? collection + '/' : '') + slug + ')' : '' }`,
        subtitle: description,
        media: media
      }
    }
  }
}
