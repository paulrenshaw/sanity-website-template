export default {
    name: 'product',
    title: 'Product',
    type: 'document',
    fields: [
        {
            name: 'title',
            title: 'Title',
            type: 'string'
        },
        {
            name: 'description',
            title: 'Description',
            type: 'string'
        },
        {
            name: 'price',
            title: 'Price',
            type: 'number'
        },
        {
            name: 'offer',
            title: 'Offer Price',
            type: 'number'
        },
        {
            name: 'preview',
            title: 'Preview',
            type: 'preview'
        }
    ]
}