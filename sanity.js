module.exports = {
  root: true,
  project: {
    name: process.env.SANITY_PROJECT_NAME,
  },
  api: {
    projectId: process.env.SANITY_PROJECT_ID,
    dataset: process.env.SANITY_DATASET,
  },
  plugins: [
    "@sanity/base",
    "@sanity/components",
    "@sanity/default-layout",
    "@sanity/default-login",
    "@sanity/desk-tool",

    "@sanity/color-input",
  ],
  env: {
    development: {
      plugins: ["@sanity/vision"],
    },
  },
  parts: [
    {
      name: "part:@sanity/base/initial-value-templates",
      path: "./initialValueTemplates"
    },
    {
      name: "part:@sanity/base/schema",
      path: "./schemas/schema",
    },
    {
      name: "part:@sanity/desk-tool/structure",
      path: "./deskStructure.js",
    },
  ],
};
