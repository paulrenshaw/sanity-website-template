const dotenv = require('dotenv-safe')
const { writeFileSync } = require('fs')

const { NODE_ENV } = process.env
dotenv.config({
    path: NODE_ENV == "development" ? './.env.development' : './.env'
})

const sanity = require('./sanity.js')
const sanityJson = JSON.stringify(sanity)

try {
    writeFileSync('./sanity.json', Buffer.from(sanityJson))
    console.log('sanity.json ready for deployment')
} catch (err) {
    console.err()
}
