import S from "@sanity/desk-tool/structure-builder";

export default () => {
  return S.list()
    .title("Menu")
    .items([
      S.listItem("page")
        .title("Home Page")
        .child(S.editor().schemaType("page").id("index").documentId("index")),
      S.listItem("page")
        .title("Main Pages")
        .child(
          S.documentTypeList("page")
            .title("Main Pages")
            .schemaType("page")
            .filter(
              '_type == "page" && !defined(collection) && !(_id == "index" || _id == "drafts.index")'
            )
        ),
      S.listItem({
        id: "collection-pages",
        title: "Collection Pages",
        schemaType: "page",
        child: () =>
          S.documentTypeList("collection").child((collectionId) =>
            S.documentTypeList("page")
              .title("Pages")
              .filter('_type == "page" && $collectionId == collection._ref')
              .params({ type: "collection", collectionId })
              .initialValueTemplates([
                S.initialValueTemplateItem("collection-page", { collectionId }),
              ])
          ),
      }),
      S.listItem("collection")
        .title("Collections")
        .child(
          S.documentTypeList("collection")
            .title("Collections")
            .schemaType("collection")
        ),
      S.listItem("category")
        .title("Categories")
        .child(
          S.documentTypeList("category")
            .title("Categories")
            .schemaType("category")
        ),
      S.listItem("product")
        .title("Products")
        .child(
          S.documentTypeList("product").title("Products").schemaType("product")
        ),
      S.listItem()
        .title("Lists")
        .child(
          S.list()
            .title("Lists")
            .items([
              S.listItem()
                .title("Collection Lists")
                .child(
                  S.documentTypeList("collectionList")
                    .title("Collection Lists")
                    .schemaType("collectionList")
                ),
              S.listItem()
                .title("Custom Lists")
                .child(
                  S.documentTypeList("customList")
                    .title("Custom Lists")
                    .schemaType("customList")
                ),
            ])
        ),
      S.listItem()
        .title("Settings")
        .child(
          S.list()
            .title("Settings")
            .items([
              S.listItem()
                .title("Site")
                .child(
                  S.editor().id("site").schemaType("site").documentId("site")
                ),
              S.listItem()
                .title("Brand")
                .child(
                  S.editor().id("brand").schemaType("brand").documentId("brand")
                ),
              S.listItem()
                .title("Font")
                .child(
                  S.editor()
                    .id("fonts")
                    .schemaType("fonts")
                    .documentId("fonts")
                ),
                S.listItem()
                .title("Color Scheme")
                .child(
                  S.documentTypeList("schemeColor")
                    .title("Colors")
                    .schemaType("schemeColor")
                ),
            ])
        ),
      S.listItem()
        .title("Forms")
        .child(
          S.list()
            .title("Forms")
            .items([
              S.listItem()
                .title("Hubspot Forms")
                .schemaType("hubspotForm")
                .child(
                  S.documentTypeList("hubspotForm").title("Hubspot Forms")
                ),
            ])
        ),
    ]);
};
