# Sanity Website Template

An Sanity Studio project with opinionated schemas for building typical website content.

A frontend which uses this to generate a static site can be found at https://github.com/paulrenshaw/gatsby-sanity-template
